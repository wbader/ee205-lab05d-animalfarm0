///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    updateCats.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "catDatabase.h"
#include "updateCats.h"
#include "reportCats.h"

int updateCatName(const int index, const char newName[])
{
	if(findCat(newName) != -1)
	{
		printf("updateCatName() - duplicate name [%s]\n", newName);
		return -1;
	}
	if(strlen(newName) < 1 || strlen(newName) > MAX_CAT_NAME_LENGTH)
	{
		printf("updateCatName() - invalid newName length passed\n");
		return -1;
	}
	if(index < 0 || index >= totalCats)
	{
		printf("updateCatName() - index [%d] out of bounds\n", index);
		return -1;
	}
	strcpy(name[index], newName);
	return 0;
}

int fixCat(const int index)
{
	if(isFixed[index] == true)
	{
		printf("[%s] was already fixed!\n", name[index]);
		return 1;
	}
	if(index < 0 || index >= totalCats)
	{
		printf("fixCat() - index out of bounds\n");
		return 1;
	}
	isFixed[index] = true;
	return 0;
}

int updateCatWeight(const int index, const float newWeight)
{
	if(newWeight <= 0.0)
	{
		printf("updateCatWeight() - newWeight [%f] <= 0\n", newWeight);
		return 1;
	}
	
	if(index < 0 || index >= totalCats)
	{
		printf("updateCatWeight() - index out of bounds\n");
		return 1;
	}
	
	weight[index] = newWeight;
	return 0;
}

