///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    updateCats.h
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "catDatabase.h"

extern int updateCatName(const int index, const char newName[]);
extern int fixCat(const int index);
extern int updateCatWeight(const int index, const float newWeight);
