///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    reportCats.h
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern void printCat(const int index);
extern void printAllCats();
extern int findCat(const char catName[]);
