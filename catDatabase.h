///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    catDatabase.h
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>
#include <stddef.h>

#define MAX_CAT_NAME_LENGTH (30)
#define MAX_CATS (100)

enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed  {UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

// Global Variables
extern char        name[MAX_CATS][MAX_CAT_NAME_LENGTH];
extern enum Gender gender[MAX_CATS];
extern enum Breed  breed[MAX_CATS];
extern bool        isFixed[MAX_CATS];
extern float       weight[MAX_CATS];

extern int         totalCats;

// function declartions
extern void initializeDatabase();
