///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    addCats.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "addCats.h"
#include "catDatabase.h"

int addCat(const char catName[], const enum Gender catGender, const enum Breed catBreed, const bool catIsFixed, const float catWeight)
{
	if(strlen(catName) < 1 || strlen(catName) > MAX_CAT_NAME_LENGTH)
	{
		printf("addCat() - catName length error\n");
		return 1;
	}

	if(catWeight <= 0)
	{
		printf("addCat() - catWeight too small\n");
		return 1;
	}
	for(int i = 0; i < totalCats; i++)
	{
		if(strcmp(name[i], catName) == 0)
		{
			printf("addCat() - catName already used\n");
			return 1;
		}
	}
	
	strcpy(name[totalCats], catName);
	gender[totalCats]     = catGender;
	breed[totalCats]      = catBreed;
	isFixed[totalCats]    = catIsFixed;
	weight[totalCats]     = catWeight;
	
	totalCats++;
	
	return 0;
}
