///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    main.c
/// @version 1.0 - Initial version
///
/// This is Animal Farm 0, we're creating a database of cats
/// using enums and arrays.  This program will be adapted further
/// to Animal Farm 4, 5 or beyond!
///
/// Compile: $ make
///
/// Usage:   $ ./animalfarm
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

char        name[MAX_CATS][MAX_CAT_NAME_LENGTH];
enum Gender gender[MAX_CATS];
enum Breed  breed[MAX_CATS];
bool        isFixed[MAX_CATS];
float       weight[MAX_CATS];

int         totalCats;

//  int argc, char* argv[] // if i need command line add this to main()
int main() 
{
	initializeDatabase();

	addCat("Leonard", MALE, SHORTHAIR, false, 13.1);
	addCat("Penny", FEMALE, SHORTHAIR, true, 9.9);
	addCat("", UNKNOWN_GENDER, SPHYNX, true, 10);  // Should fail
	addCat("Recruit", MALE, SHORTHAIR, true, 0);  // Should fail
	
	printAllCats();
	
	printf("\n");
	
	int testCat = findCat("Recruit");  // Should fail
	if(testCat != -1)
		printCat(testCat);
		
	addCat("Recruit", MALE, SHORTHAIR, true, 10.4);  // Put the cat in the database
	
	testCat = findCat("Recruit");
	if(testCat != -1)
		printCat(testCat);
		
	updateCatName(2, "Big cat");
	printCat(2);
	
	printf("\n");
	
	updateCatName(2, "Leonard"); // Should fail
	updateCatName(3, "Recurit"); // Should fail
	updateCatName(1, "");		 // Should fail
	
	printf("\n");	
	
	printCat(0);
	fixCat(0);
	printCat(0);
	fixCat(0);
	
	printf("\n");
	
	printCat(2);
	updateCatWeight(2, 13.0);
	printCat(2);
	
	updateCatWeight(15, 2.0);  // Should fail
	
	printCat(1);
	updateCatWeight(1, -15.2); // Should fail
	printCat(1);
	
	printf("\n");
	
	printAllCats();
	deleteCat(0);
	printAllCats();
	deleteAllCats();
	printAllCats();
}
