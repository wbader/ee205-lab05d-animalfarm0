///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    catDatabase.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"

void initializeDatabase()
{
	for(int i = 0; i < MAX_CATS; i++)
	{
		name[i][0] = 0;
		weight[i]  = 0.0;
	}
	
	totalCats = 0;
	
}
