///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    reportCats.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "reportCats.h"
#include "catDatabase.h"


void printCat(const int index)
{
	if(index < 0 || index >= totalCats)
	{
		printf("animalFarm0: Bad cat [%d]", index);
		return;
	}
	
	printf("cat index = [%d]  name = [%s] gender = [%d] breed = [%d] isFixed = [%d] weight = [%f]\n",
			index,
			name[index],
			gender[index],
			breed[index],
			isFixed[index],
			weight[index]);
			
	return;	
}

void printAllCats()
{
	for(int i = 0; i < totalCats; i++)
	{
		printCat(i);
	}
	return;
}


int findCat(const char catName[])
{
	for(int i = 0; i < totalCats; i++)
	{
		if(strcmp(catName, name[i]) == 0)
		{
			return i;
		}
	}
	return -1; 
}
