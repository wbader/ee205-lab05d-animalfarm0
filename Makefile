###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a cat database
###
### @author  Waylon Bader <wbader@hawaii.edu> 
### @date    22 Feb 2021 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall -Wextra

TARGET = animalfarm

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c
	
reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c
	
addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h reportCats.h
	$(CC) $(CFLAGS) -c updateCats.c
	
deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h reportCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c
	
animalfarm: main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

test: animalfarm
	./animalfarm

clean:
	rm -f $(TARGET) *.o

