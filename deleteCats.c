///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    deleteCats.c
/// @version 1.0 - Initial version
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats()
{
	totalCats = 0;
}

void deleteCat(const int index)
{
	if(index < 0 || index >= totalCats)
	{
		printf("deleteCat() - index out of bounds\n");
		return;
	}
	
	for(int i = index; i < totalCats - 1; i++)
	{
		strcpy(name[i], name[i+1]);
		gender[i] = gender[i+1];
		breed[i] = breed[i+1];
		isFixed[i] = isFixed[i+1];
		weight[i] = weight[i+1];
	}
	
	totalCats = totalCats - 1;
}
